import TicTacToeGame.{Board, Mark}

sealed trait Player{
  def other: Player = this match {case X => O; case _ => X}
  override def toString: String = this match {case X => "X"; case _ => "O"}
}
case object X extends Player
case object O extends Player

case class Cell(x:Int,y:Int)

object Cell {
  implicit def tupleToCell(tuple:(Int,Int)):Cell ={
    Cell(tuple._1,tuple._2)
  }
  implicit def cellToTuple(cell: Cell):(Int,Int) ={
    (cell.x,cell.y)
  }
}
sealed trait TicTacToeGame{
  def isGameBoardOver:Boolean
  def getWinnerIfPresent:Option[Player]
  def addMoveToBoardIfPossible(cell: Cell,player: Player):Boolean
  def computeAiMove:(Cell,Player)
  def resetGame():Unit
  def printBoard():Unit
}

object TicTacToeGame extends TicTacToeGame {
  import GameUtility._
  import BoardFunctions._
  var turn:Player = player1
  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]
  var gameBoard:Board = List()
  override def isGameBoardOver: Boolean = isBoardOver(gameBoard)

  override def getWinnerIfPresent: Option[Player] = getWinningPlayer(gameBoard)

  override def addMoveToBoardIfPossible(cell: Cell,player: Player):Boolean = find(gameBoard,cell.x,cell.y) match {
    case None if turn==player && !isGameBoardOver =>
      gameBoard = Mark(cell.x,cell.y,player)::gameBoard
      turn = turn.other
      true
    case _=>  false
  }

  override def computeAiMove:(Cell,Player)={
    val aiMove =miniMax(gameBoard,player2)._1
    gameBoard = aiMove::gameBoard
    turn =turn.other
    ((aiMove.x,aiMove.y),turn.other)
  }

  override def resetGame():Unit = {
    gameBoard = List()
    turn=player1
  }

  override def printBoard(): Unit = printBoards(gameBoard)

}

private[this] object BoardFunctions{
  import GameUtility._
  def find(board: Board, x: Double, y: Double): Option[Player] = {
    board
      .filter(mark=>mark.x==x && mark.y==y)
      .map(mark=>mark.player)
      .headOption
  }
  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    Stream
      .range(0,column)
      .flatMap(
        x=>Stream.range(0,rows)
          .filter(y=>find(board,x,y).isEmpty)
          .map(y=>Mark(x,y,player)::board)
      )
      .toList
  }

  def getWinningPlayer(board: Board):Option[Player]={
    def _check(f:Mark=>Boolean,g: Mark=>(Any,Player)):Option[Player] = board
      .filter(f)
      .groupBy(g)
      .filter(m=>m._2.size==column)
      .map(pair=>pair._1._2)
      .headOption

    val nullFilter:Mark=>Boolean = _=>true
    val groupByRow:Mark=>(Any,Player) = mark=>(mark.x,mark.player)
    val groupByColumn:Mark=>(Any,Player) = mark=>(mark.y,mark.player)
    val groupByPlayer:Mark=>(Any,Player) = mark=>(Nil,mark.player)
    val filterByDiag1:Mark=>Boolean = mark=>mark.x==mark.y
    val filterByDiag2:Mark=>Boolean = m=> m.x+m.y==column-1
    _check(nullFilter,groupByRow)
      .orElse(
        _check(nullFilter,groupByColumn)
          .orElse(
            _check(filterByDiag1,groupByPlayer)
              .orElse(
                _check(filterByDiag2,groupByPlayer)
                  .orElse(Option.empty[Player])
              )
          )
      )
  }
  def isBoardOver(board: Board): Boolean = getWinningPlayer(board) match {
    case Some(_) => true
    case None if board.size==column*rows => true
    case _ => false
  }

  def getScoreFromBoard(board: Board):Int =getWinningPlayer(board) match {
    case Some(`player2`) => 10
    case Some(`player1`) => -10
    case None if board.size==column*rows => 0
  }

  def getBestMoves(seq: Seq[(Mark,Int)],player: Player):(Mark,Int)  = player match {
    case `player2` => seq.maxBy(_._2)
    case `player1` => seq.minBy(_._2)
  }
  def miniMax(board: Board, player: Player) :(Mark,Int)= isBoardOver(board) match{
    case true =>(board.head,getScoreFromBoard(board))
    case false =>
      val availSpots = placeAnyMark(board,player)
      getBestMoves(availSpots.map(b=>(b.head,miniMax(b,player.other)._2)),player)
  }
  def printBoards(board: Board): Unit =
    for (y <- 0 until column;x <- 0 until rows) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == rows-1) { print(" "); println()}
    }
}