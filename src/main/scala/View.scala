import java.awt.{Color, Dimension, FlowLayout, GridLayout}
import java.awt.event.{ActionEvent, ActionListener}

import javax.swing.{JButton, JFrame, JOptionPane, JPanel}

sealed trait View {
  def updateCell(cell:(Int,Int),player: Player):Unit
  def resetBoard():Unit
  def printMessageDialog(string: String):Unit
  def addObserver(controller: Controller):Unit
  def getModality:Option[Modality]
  def showView():Unit
}

object View extends View {
  import GameUtility._
  import ScalaSwingHelper._
  val f = new JFrame(appName)
  var turnOfPlayer:Player = player1
  var observers:List[Controller] = List()

  val choices: Array[AnyRef] = Array(HumanVsAi.modalityName, HumanVsHuman.modalityName)
  val questionText:String = "In which mode do you want to play?"

  val choice: Int = showOptionPaneAndGetResult(questionText,choices, choices(0))
  val buttons:Map[(Int,Int),JButton]  = Stream
    .range(0,column)
    .flatMap(
      y=>Stream
        .range(0,rows)
        .map(x=>{
          val button = new JButton()
          addButtonsToMainPanel(button)
          ((x,y),button)
        }
        )
    )
    .toMap

  buttons.foreach(entry=>entry._2.addActionListener((e:ActionEvent)=>{
    observers.foreach(_.notifyCellClicked(entry._1,turnOfPlayer))
  }))

  override def getModality: Option[Modality] = _getModalityByChoice(choice)

  private def _getModalityByChoice(choice:Int):Option[Modality] = choice match {
    case 0 => Some(HumanVsAi)
    case 1 => Some(HumanVsHuman)
    case _=> None
  }
  override def addObserver(controller: Controller): Unit = observers= controller ::observers

  override def updateCell(cell: (Int,Int), player: Player): Unit = {
    buttons((cell._1,cell._2)).setBackground(getColorByPlayer(player))
    turnOfPlayer =player.other
  }

  override def resetBoard(): Unit ={
    buttons.values.foreach(b=>b.setBackground(defaultColor))
    turnOfPlayer = player1
  }

  override def printMessageDialog(string: String):Unit = {
    def _playAgain(res:Int):Unit = res match{
      case 0 => observers.foreach(_.playNewGame())
      case _ =>
    }
    val message:String = string+ "\nDo you want to play again?"
    _playAgain(showOptionPaneAndGetResult(message,null,null))
  }

  override def showView(): Unit = f.setVisible(true)

  f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  f.getContentPane.setLayout(new FlowLayout())
  f.getContentPane.add(getMainPanel)
  f.pack()



}

object ScalaSwingHelper {
  import GameUtility._
  val defaultColor:Color = new JButton().getBackground

  val mainPanel = new JPanel()
  mainPanel.setPreferredSize(new Dimension(400,400))
  mainPanel.setLayout(new GridLayout(rows,column))
  def addButtonsToMainPanel(button:JButton): Unit = {
    mainPanel.add(button)
  }
  def getMainPanel:JPanel = mainPanel
  implicit def funToActionListener(f: (ActionEvent)=>Unit): ActionListener =
    new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit = f(e)
    }
  implicit def unitToActionListener(f: =>Unit): ActionListener =
    new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit = f
    }
  def showOptionPaneAndGetResult(message:String,choices:Array[AnyRef],initialValue:AnyRef):Int={
    JOptionPane.showOptionDialog(null, message, appName, JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, choices, initialValue)
  }

}
