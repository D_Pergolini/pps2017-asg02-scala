import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object Main extends App{
  val view: View = View
  if (view.getModality.isEmpty)
    System.exit(0)
  else
    Controller(view,view.getModality.get)
}


sealed trait Controller{
  def notifyCellClicked(cell: Cell,player: Player):Unit
  def playNewGame():Unit
}

object Controller{
  def apply(view: View,modality: Modality): Controller = new ControllerImpl(view,modality)
  import ExecutionContext.Implicits.global

  private class ControllerImpl(val view: View,val modality: Modality) extends Controller {
    import GameUtility._
    view.addObserver(this)
    view.showView()
    val game: TicTacToeGame = TicTacToeGame
    var endMessages:EndMessages = EndMessages(modality)
    override def playNewGame(): Unit = {
      view.resetBoard()
      game.resetGame()
    }

    private def addMoveToBoardIfPossible(cell: Cell, player: Player): Unit =
      if(game.addMoveToBoardIfPossible(cell, player)){
        view.updateCell(cell,player)
      }

    private def computeAiMoveAndUpdateView(): Unit = {
      Future {
        game.computeAiMove
      }.onComplete {
        case Success(aiMove: (Cell, Player)) =>
          view.updateCell(aiMove._1, aiMove._2)
          checkIfGameIsOverAndPrintMessage

        case Failure(exception) => println("Error")
      }
    }

    override def notifyCellClicked(cell: Cell, player: Player): Unit = modality match {
      case HumanVsAi if player != player2 =>
        addMoveToBoardIfPossible(cell,player)
        if(!checkIfGameIsOverAndPrintMessage){
            computeAiMoveAndUpdateView()
        }
      case HumanVsHuman =>
        addMoveToBoardIfPossible(cell, player)
        checkIfGameIsOverAndPrintMessage
      case _ =>
    }

    def checkIfGameIsOverAndPrintMessage: Boolean = game.getWinnerIfPresent match {
      case Some(`player2`) =>
        view.printMessageDialog(endMessages.player2WinMessage)
        true
      case Some(`player1`) =>
        view.printMessageDialog(endMessages.player1WinMessage)
        true
      case _ if game.isGameBoardOver =>
        view.printMessageDialog(endMessages.drawMessage)
        true
      case _ => false
    }
  }


}