import java.awt.Color

object GameUtility{
  val appName:String = "Tic Tac Toe"
  val rows:Int = 3
  val column:Int = 3
  val player1:Player=O
  val player2:Player=X
  def getColorByPlayer(player: Player):Color = player match {
    case `player1` => Color.RED
    case `player2` => Color.BLUE
  }
}
sealed trait Modality{
  def modalityName:String
}

case object HumanVsAi extends Modality {
  override def modalityName: String = "Human VS AI"
}
case object HumanVsHuman extends Modality {
  override def modalityName: String = "Human vs Human"
}

sealed trait EndMessages{
  def drawMessage:String
  def player1WinMessage:String
  def player2WinMessage:String

}

object EndMessages{
  def apply(modality:Modality): EndMessages = new EndMessagesImpl(modality)
  private class EndMessagesImpl(val modality:Modality) extends EndMessages{
    val aiWinMessage:String = "AI is too smart!You Lose."
    val aiLoseMessage:String= "Human are still better than AI!You Win!"
    val drawMsg:String = "It is a draw!"
    val player1WinMsg:String ="Congratulations Player 1, You Win!"
    val player2WinMsg:String ="Congratulations Player 2, You Win!"
    override def drawMessage: String = drawMsg

    override def player1WinMessage: String = modality match {
      case HumanVsAi => aiLoseMessage
      case HumanVsHuman => player1WinMsg
    }

    override def player2WinMessage: String = modality match {
      case HumanVsAi => aiWinMessage
      case HumanVsHuman => player2WinMsg
    }

  }
}