import org.scalatest.{Outcome, fixture}


class SetSuite extends fixture.FunSuite {
  override type FixtureParam = TicTacToeGame

  override protected def withFixture(test: OneArgTest): Outcome = {
    val game:TicTacToeGame = TicTacToeGame
    test(game)
  }

  test("Test if game is over at the beginning and at the end of game"){ game=>
    game.resetGame()

    assert(!game.isGameBoardOver)
    addMovesToWinPlayerX(game)
    assert(game.isGameBoardOver)
  }

  test("Test the add of a mark"){ game =>
    game.resetGame()

    val moveToAdd:(Int,Int,Player) = (0,0,O)
    assertResult(true)(game.addMoveToBoardIfPossible((moveToAdd._1,
      moveToAdd._2),
      moveToAdd._3))
    assertResult(false)(game.addMoveToBoardIfPossible((moveToAdd._1,
      moveToAdd._2),
      moveToAdd._3.other))
  }
  test("Test if the AI do the smartest moves after the best first possible move"){ game=>
    game.resetGame()

    game.addMoveToBoardIfPossible((0,0),O)
    assertResult(Cell(1,1))(game.computeAiMove._1)
  }
  test("Test the reset of a board after that is over"){game =>
    game.resetGame()

    addMovesToWinPlayerO(game)
    assertResult(true)(game.isGameBoardOver)
    game.resetGame()
    assertResult(false)(game.isGameBoardOver)

  }
  test("Test if the returned winner player is right"){game=>
    game.resetGame()

    assertResult(None)(game.getWinnerIfPresent)
    addMovesToWinPlayerO(game)
    assertResult(Some(O))(game.getWinnerIfPresent)
    game.resetGame()
    addMovesToWinPlayerX(game)
    assertResult(Some(X))(game.getWinnerIfPresent)
  }

  def addMovesToWinPlayerX(game: TicTacToeGame): Unit ={
    game.addMoveToBoardIfPossible((0,0),O)
    game.addMoveToBoardIfPossible((2,0),X)
    game.addMoveToBoardIfPossible((1,1),O)
    game.addMoveToBoardIfPossible((2,1),X)
    game.addMoveToBoardIfPossible((0,2),O)
    game.addMoveToBoardIfPossible((2,2),X)
  }
  def addMovesToWinPlayerO(game: TicTacToeGame): Unit ={
    game.addMoveToBoardIfPossible((0,0),O)
    game.addMoveToBoardIfPossible((1,0),X)
    game.addMoveToBoardIfPossible((0,1),O)
    game.addMoveToBoardIfPossible((2,0),X)
    game.addMoveToBoardIfPossible((0,2),O)
  }

}

