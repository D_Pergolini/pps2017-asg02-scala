name := "pps2017-asg02-scala"

version := "0.1"

scalaVersion := "2.12.6"

val scalatest = "org.scalatest" %% "scalatest" % "3.0.5" % "test"
lazy val root = ( project in file (".")).settings(libraryDependencies ++= Seq ( scalatest ))